// Manual import of modules from package
/*import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";*/

// Object Desctructuring
import {Button, Container, Row, Col} from 'react-bootstrap';
	// react-bootstrap here is the website itself

import {Link} from 'react-router-dom'

export default function banner() {
	
	return (
		<Container>
			<Row>
				<Col className = 'mt-3 text-center'> 
					<h1>Zuitt Coding Bootcamp</h1>
					<p>Opportunities for everyone everywhere</p>
					<Button as = {Link} to = '/courses'>Enroll Now!</Button>
				</Col>
			</Row>
		</Container>
		)

}

// classes in bootstrap work the same way here, however, instead of 'class =', it should be 'className ='