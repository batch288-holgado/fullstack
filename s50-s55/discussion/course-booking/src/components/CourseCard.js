import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
// import the useState hook from react
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'
import Swal2 from 'sweetalert2'

export default function CourseCard(props) {
	// the 'props' argument here will be the '{coursesData}' in Courses.js, which is essentially the mock database

	const{user} = useContext(UserContext)

	const {_id, name, description, price} = props.courseProp
	// courseProp here is a property defined in Courses.js page, doing this above is called 'object destructuring' wherein you separate the elements of courseProp into {_id}, {name}, {description}, {price} 
	// alternatively you can read this as: from props.courseProp, desctructure the elements as stated
	// this is why we can use {_id} below

	// Use the state hook for this component to store its state; states are used to keep track of information related to individual components
		// Syntax: const [getter, setter] = useState(initialGetterValue)

	// Syntax: useEffect(sideEffect, [dependencies])
		// useEffect looks for any changes in the dependency/ies

// NOTE THAT YOU HAVE TO IMPORT BOTH useState and useEffect from 'react' ABOVE

	return (
		<Container>
			<Row>
				<Col>
					<Card className = 'mt-3'>
						<Card.Body>
							
							<Card.Title>{name}</Card.Title>
							
							<Card.Subtitle className = 'mt-3'>Description:</Card.Subtitle>
							
							<Card.Text>
							{description}
							</Card.Text>

							<Card.Subtitle className = 'mt-3'>Price:</Card.Subtitle>

							<Card.Text>
							Php {price}
							</Card.Text>

							{
								user !== null 
								?
								<Button variant="primary" as = {Link} to = {`/courses/${_id}`}>Details</Button>
								// this ${_id} will be retrieved using useParams in CourseView
								:
								<Button variant="primary" as = {Link} to = '/login'>Login to Enroll</Button>
							}

						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}