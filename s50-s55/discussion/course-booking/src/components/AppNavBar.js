import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {useState, useContext} from 'react'
import UserContext from '../UserContext'

// We use Link and NavLink to add hyperlink/s to our application; NavLink is for NavBars

// The 'as' allows components to be treated as if they are a different component gaining the access to its properties and functionalities
import {Link, NavLink} from 'react-router-dom';

export default function AppNavBar(){

	// const [user, setUser] = useState(localStorage.getItem('email')) 

	// NOTE that since there is already a value for email in the 'localStorage.setItem' in the login.js, we do not need to setItem again here

	const {user, setUser} = useContext(UserContext)
	// NOTE that useContext is imported from react, and UserContext is imported from UserContext.js

	return( 
	//create this function with this return() and just paste from react-bootstrap the code of the component you want

		<>
			<Navbar bg="dark" variant="dark">
		    	<Container>
		        	<Navbar.Brand as = {Link} to = '/'>Zuitt</Navbar.Brand>
			        <Nav className="ms-auto">
			        	<Nav.Link as = {NavLink} to = '/'>Home</Nav.Link>
			        	<Nav.Link as = {NavLink} to = '/courses'>Courses</Nav.Link>
			        	{
			        		user.id === null ?
			        		<>
			        			<Nav.Link as = {NavLink} to = '/register'>Register</Nav.Link>
			        			<Nav.Link as = {NavLink} to = '/login'>Login</Nav.Link>
			        		</> 
			        		: 
			        		<Nav.Link as = {NavLink} to = './logout'>Logout</Nav.Link>
			        	}
			        </Nav>
				</Container>
			</Navbar>     
		</>

		)

}