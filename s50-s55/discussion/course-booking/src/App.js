import './App.css';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home'
import Courses from './pages/Courses'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import NotFound from './pages/NotFound'
import CourseView from './pages/CourseView'

// import Name is user defined if you made your own function, however if you will import from react-bootstrap, you have to use the name itself (e.g. Container, Row, Column, etc...)
{
  /* The following line can be included in your src/index.js or App.js file */
}

// Importing UserProvider
import {UserProvider} from './UserContext.js'

import 'bootstrap/dist/css/bootstrap.min.css';
import {useState, useEffect} from 'react'
import {BrowserRouter, Route, Routes} from 'react-router-dom';
/*
  - BrowserRouter: Enables stimulation of page navigation by synchronizing the content shown with the url in the web browser

  - Routes: Holds all our Route components. It selects which 'Route' component to show based on the URL endpoint.
*/

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

/*useEffect(()=>{
  console.log(user);
  console.log(localStorage)
}, [user])*/

useEffect(()=>{
  if(localStorage.getItem('token')){
    fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
    })  
  }
},[])
// this is an 'if' condition since there will be an uncaught promise if you invoke the function in this useEffect with some elements (such as the localStorage.getItem('token')) being null

  return (
    <UserProvider value = {{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavBar/>

        <Routes>
            <Route path = '/' element = {<Home/>} />
            <Route path = '/courses' element = {<Courses/>} />
            <Route path = '/register' element = {<Register/>} />
            <Route path = '/login' element = {<Login/>} />
            <Route path = '/logout' element = {<Logout/>} />
            <Route path = '/courses/:courseId' element = {<CourseView/>} />
            <Route path = '*' element = {<NotFound/>} />
        </Routes>

      </BrowserRouter>
    </UserProvider>

      // Note that in return(), only one component can be done in a time, so in order to return multiple components in one return(), we can put them inside a <div></div> tag or a react fragment -> <></>
  )
}

export default App;