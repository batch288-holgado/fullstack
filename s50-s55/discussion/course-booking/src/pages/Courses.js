import coursesData from '../data/courses'
import CourseCard from '../components/CourseCard'
import {useEffect, useState} from 'react'

export default function Courses () {
	
	// we console the coursesData to check successful mounting of the database
	// console.log(coursesData)

	// the courseProp will give us an object with property 'courseProp' and the values of data in an array; NOTE that 'courseProp' is defined by the user to be the name of the object, whereas data is from fetched database (see CourseCard for courseProp application)
	const [courses, setCourses] = useState([])
	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/getActiveCourses`)
		.then(result => result.json())
		.then(data => {
			setCourses(data)
		})
	},[])

	const course = courses.map(course => {
		return(
			<CourseCard key = {course._id} courseProp = {course}/>
			)
	})

	return(
		<>
		<h1 className = 'text-center mt-3'>Courses</h1>
		{course}
		</>
	)
}

// NOTE that the "key" is not required, however, it will cause a warning error in console, saying that each iteration should have a unique key. Putting this key solves that.