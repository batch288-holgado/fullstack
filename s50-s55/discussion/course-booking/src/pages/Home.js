import Banner from '../components/Banner'
import Highlights from '../components/Highlights'
// import CourseCard from '../components/CourseCard'

export default function home () {

	return (
		<>
		<Banner/>
		<Highlights/>
		{/*<CourseCard/>*/}
		</>
	)

}