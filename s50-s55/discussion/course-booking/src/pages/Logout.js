import {Navigate} from 'react-router-dom'
import {useState, useContext, useEffect} from 'react'
import UserContext from '../UserContext'

export default function Logout () {

	// To clear out the content of our local storage, we only have to use the clear() method
	// localStorage.clear()

	const {unsetUser, setUser} = useContext(UserContext)
	useEffect(() => {
		unsetUser();
		setUser({
			id: null,
			isAdmin: null
		})
	})

	return(
		<Navigate to = '/login'/>
	)

}