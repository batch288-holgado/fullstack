import React from 'react'
import {Link} from 'react-router-dom'

export default function NotFound() {
	return (
		<div className = 'text-center m-3'>
			<h1 style = {{fontSize: '30px'}}>Zuitt Booking</h1>
			<img className = 'w-50' src = 'https://static.vecteezy.com/system/resources/thumbnails/008/255/803/small/page-not-found-error-404-system-updates-uploading-computing-operation-installation-programs-system-maintenance-a-hand-drawn-layout-template-of-a-broken-robot-illustration-vector.jpg'/>
			{/*<h1>Not Found</h1>*/}
			<p>Go back to <Link to = '/' style = {{textDecoration: 'none'}}>homepage</Link></p>
		</div>
	)
}