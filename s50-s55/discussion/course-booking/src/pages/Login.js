import {Container, Row, Col, Button, Form} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Navigate, Link, useLocation, useNavigate} from 'react-router-dom'
import NotFound from './NotFound'
import Swal2 from 'sweetalert2'
import UserContext from '../UserContext.js'
// NOTE: useContext is a hook from react; UserContext from the UserContext.js that we made

export default function login () {

	const [isDisabled, setIsDisabled] = useState(true)
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const location = useLocation()
	const navigate = useNavigate()

	const {user, setUser} = useContext(UserContext)
	// the useState() of user and setUser here is the one defined in app.js -> this is the purpose of this createContext, so that we won't have to create a new state in every page.

// 
	const retreiveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
		})
	}

	useEffect(()=>{

		if (email !== '' && password !== ''){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}

	}, [email, password])

	useEffect(()=>{
		if (location.pathname === '/login' && localStorage.getItem('token')){
			navigate('*')
		}
	},[location.pathname, navigate])

	function login (event) {
		event.preventDefault()

		// Processing a fetch request corresponding to the backend API (Syntax: fetch('url', {options}))
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			if (data === false){
				Swal2.fire({
					title: 'Authentication Failed!',
					icon: 'error',
					text: 'Check your login details and try again'
				})
			}else{
				// alert('login is successful');
				localStorage.setItem('token', data.auth);
				// data.auth is used since 'data' is an object with a property 'auth' inside whose value is the actual token (see usersControllers -> loginUser function)

				// setUser(localStorage.getItem('token'))
				retreiveUserDetails(data.auth)
				// data.auth here is now a token, which becomes the argument used for the retrieveUserDetails function where it is decoded

				Swal2.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt'
				})
			}
		})

		/*console.log(user)
		alert('You are now logged in!')*/

		/*Set the email of the authenticated user in the local storage 
			- Syntax: localStorage.setItem('propertyName', value)
			- propertyName = key; value = Value in inspect -> Application -> Local Storage*/

		/*localStorage.setItem('email', email)
		setUser(localStorage.getItem('email'))
		setEmail('')
		setPassword('')*/
	}

	return (

	(user.id === null) ?

		<Container>
			<Row>
				<Col className = 'col-6 mt-3 mb-3 mx-auto'>
					<h1 className = 'text-center'>Login</h1>
					<Form onSubmit = {event => login(event)}>

						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control 
								type="email" 
								value = {email} 
								onChange = {		
									event => setEmail(event.target.value)}
								placeholder="Enter email" 
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password" 
								placeholder="Password" 
								value = {password}
								onChange = {event => setPassword(event.target.value)}
							/>
						</Form.Group>

						<p>No account yet? <Link to = '/register' style = {{textDecoration: 'none'}}>Sign up here</Link></p>

						<Button variant="primary" type="submit" disabled={isDisabled}>
						Submit
						</Button>
						
					</Form>
				</Col>
			</Row>
		</Container> 
		
		: 
		
		<Navigate to = '/'/>
	)
}