import {Container, Row, Col, Button, Form} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Link, useNavigate, useLocation, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal2 from 'sweetalert2'

export default function Register () {

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')
	const [mobileNo, setMobileNo] = useState('')

	const {user, setUser} = useContext(UserContext)

	const location = useLocation()
	const navigate = useNavigate()
	// be careful, useNavigate module is in react-router-dom, not in react, unlike useState and useEffect

	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(() => {

		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && password1.length > 6){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}

	}, [email, password1, password2])

	useEffect(()=>{
		if (location.pathname === '/register' && localStorage.getItem('token')){
			navigate('*')
		}
	}, [location.pathname, navigate])

	function register (event) {
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1,
				mobileNo: mobileNo
			})
		})
		.then(result => result.json())
		.then(data => {

			console.log(data)
			
			if (data){
				localStorage.setItem('email', email)
				setUser(localStorage.getItem('email'))

				Swal2.fire({
					title: 'Registration Sucessful',
					icon: 'success',
					text: 'Thank you for registering!'
				})

				navigate('/login')	
			}else{
				Swal2.fire({
					title: 'Email is already taken',
					icon: 'error',
					text: 'Please choose a different email'
				})
				setFirstName('')
				setLastName('')
				setEmail('')
				setMobileNo('')
				setPassword1('')
				setPassword2('')
			}
			

		})

	}

	return (
		<Container>
			<Row>
				<Col  className = 'col-6 mx-auto'>
					<h1 className = 'mt-3 text-center'>Register Now</h1>
					<Form onSubmit = {event => register(event)}>
						
						<Form.Group className="mb-3" controlId="formFirstName">				
							<Form.Label>First Name</Form.Label>
							<Form.Control 
								type = "firstName"
								value = {firstName}
								onChange = {event => setFirstName(event.target.value)}
								placeholder="First Name" 
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formLastName">
							<Form.Label>Last Name</Form.Label>
							<Form.Control 
								type = "lastName"
								value = {lastName}
								onChange = {event => setLastName(event.target.value)}
								placeholder="Last Name" 
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control 
								type="email" 
								value = {email}
								onChange = {event => setEmail(event.target.value)
								} 
								// when the 'onChange' event occurs, an 'event' object will be made in the console with a 'target' value inside
								placeholder="Enter email"
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formMobileNo">
							<Form.Label>Mobile Number</Form.Label>
							<Form.Control 
								type = "number"
								value = {mobileNo}
								onChange = {event => setMobileNo(event.target.value)}
								placeholder = "Mobile Number" 
							/>
							<Form.Text className = "text-muted">
								Please enter a valid 11-digit mobile number
							</Form.Text>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword1">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password" 
								value = {password1} 
								onChange = {event => setPassword1(event.target.value)}
								placeholder="Password"
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword2">
							<Form.Label>Confirm Password</Form.Label>
							<Form.Control 
								type="password" 
								value = {password2} 
								onChange = {event => setPassword2(event.target.value)}
								placeholder="Retype your nominated password"
							/>
						</Form.Group>

						<p>Already have an account? <Link to = '/login' style = {{textDecoration: 'none'}}>Login here</Link></p>

						<Button variant="primary" type="submit" disabled = {isDisabled}>
						Submit
						</Button>

					</Form>
				</Col>
			</Row>
		</Container>
	)
}