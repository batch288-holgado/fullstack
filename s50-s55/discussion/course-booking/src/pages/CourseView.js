import {useState, useEffect} from 'react';
import {Container, Row, Col, Button, Card} from 'react-bootstrap';
import Swal2 from 'sweetalert2'
import {useParams, useNavigate} from 'react-router-dom'

export default function CourseView() {

	const [name, setName] = useState('')
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [count, setCount] = useState(0)
	const [slot, setSlot] = useState(30)
	const [isDisabled, setIsDisabled] = useState(false)
	const navigate = useNavigate()

	const {courseId} = useParams()

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		
		// this courseId is from the useParams() above which retrieves the params from the URL 

		.then(result => result.json())
		.then(data => {
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})
	}, [])

	/*useEffect(()=>{
		if (slot === 0){
			setIsDisabled(true)
			Swal2.fire({
				title: 'Enrollment Closed',
				icon: 'info',
				text: 'No more seats available. You are the last enrollee.'
			})
		}
	}, [slot])
*/
	const enroll = (courseId) => {
		
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: 'POST',
			headers: {
				'Authorization' : `Bearer ${localStorage.getItem('token')}`,
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				id: courseId,
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if (data) {

				Swal2.fire({
					title: 'Successfully enrolled! ',
					icon: 'success',
					text: 'You have sucessfully enrolled for this course'
				})
				navigate('/courses')
				
			}else{
				Swal2.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
		
	}

	return(

		<Container className = 'mt-3'>
			<Row>
				<Col>
					<Card style={{ width: '100%' }} className = 'mx-auto'>

						<Card.Body>
							<Card.Title className = 'mb-3'>{name}</Card.Title>
							
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>{price}</Card.Text>

							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8am-5pm</Card.Text>

							{/*<Card.Subtitle>No. of Slots Available: {slot}</Card.Subtitle>*/}

							<Button variant="primary" onClick = {() => enroll(courseId)}>Enroll</Button>

						</Card.Body>

					</Card>
				</Col>
			</Row>
		</Container>
	)
}