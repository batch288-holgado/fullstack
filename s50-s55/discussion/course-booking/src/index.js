import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// createRoot() -> assigns the element to be managed by React to its virtual DOM 

const root = ReactDOM.createRoot(document.getElementById('root'));

// render() -> this is responsible for displaying the component/react element into the root

root.render(

  <React.StrictMode>
    <App />
  </React.StrictMode>

);

/*const name = 'John Smith';
const element = <h1> Hello, I am {name}! </h1>
root.render(element);*/

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

