import React from 'react'

// Creating a Context Object	
/*
	- A Context Object, as the name states, is data that can be used to store information that can be shared to other component/s within the app.

	- The context object is a different approach to passing information between components and allows easier access by avoiding the use of prop passing
*/

// With the help of the React.createContext() method, we created a context which was stored in the variable UserContext
const UserContext = React.createContext()

// The 'Provider' component allows other components to consume/use the context object and supply necessary information needed to the context object
export const UserProvider = UserContext.Provider 
// the values to be provided will be the useState() defined in the same page, above the section of the provider (see App.js)

export default UserContext
// export default basically makes the variable the automatic import if imported in another page; in this case, if you want to import UserProvider, is has to be 'import {UserProvider} from './'