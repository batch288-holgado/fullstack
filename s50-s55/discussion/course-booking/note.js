// If you will create a react.js application, do the following

/*
	- npx create-react-app project-name

	- Note that react.js is strict with the application name; it should all be in lower case
		- 'courseBooking' - this project name will cause an error

	- 'npm start' in the project folder to run the app

	- Files to be deleted (LAIR in src folder):
		- logo.svg
		- App.test.js
		- index.css
		- reportWebVitals.js
		- delete as well their corresponding import files (see error log in app)
		- in app.js, delete the elements inside the <div></div> tag as well
	
	- in package.json -> delete 'eslintConfig'

	- you may also delete the contents inside app.css (but do NOT delete the file itself)

	- in index.js -> ctrl shift P -> install package control -> package control -> install JS babel -> open files in JS babel (view -> syntax -> javascript (babel))

	- To install bootstrap: npm install react-bootstrap, npm install bootstrap

	- To install react-router-dom: npm install react-router-dom

	- THINGS TO INSTALL:
		- npm install react-bootstrap
		- npm install bootstrap
		- npm install react-router-dom
		- npm install sweetalert2

	- When making a new file -> 
	export default function functionName() {
		return(
			*paste here the codes from react bootstrap of the component you want in your app*
			- copy paste first the imports and place them with the other imports above
			- copy paste only everything inside 'return' in here
			- export as necessary
		)
	}
	- To change the port, go to package.json -> scripts -> find 

		"set port: xxxx &&" in "scripts", and change it to -> 
		"start" : "set PORT=xxxx && react-scripts start"

	- courses.js here will serve as our mock database instead of mongo.db since we want to focus on react concepts

	- <Link  to = './routeName'></Link> and <NavLink to = './routeName'></NavLink> are tags that we can use to set words as links, the difference between them is that NavLink allows customization; these are used inside of functions

	- <Navigate to = '/routeName' />  on the other hand is used as functions; such that when this function is invoked, we will be directed to the link prescribed

	- The routeNames are established in
		<BrowserRoute>
			<Routes>
				<Route path = './linkName' element = {importedPage} />
			</Routes>
		</BrowserRoute>
*/

/*
Environment Variables
	- These are important for hiding sensitive pieces of information like the backend API

	- this is setup in .env.local that you will make (see file for reference)

	- after it is setup, you may then use `${REACT_NAME}` throughout the entire app

	- you have to restart the server after refactoring for this to work
*/