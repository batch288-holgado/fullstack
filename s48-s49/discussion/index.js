// Fetch Keyword
	// fetch('url', {options})

// GET Post Data
fetch('https://jsonplaceholder.typicode.com/posts')
.then(result => result.json())
.then(response => {

	// The response is an array of objects
	console.log(response)
	showPosts(response)

})

// What happened here is we fetched the data in the url and stored them in result. We then converted the data into a json object using 'result.json()' and stored all of them in response. We then consoled it and used it as an argument in the showPosts() function

const showPosts = (posts) => {
	console.log(typeof posts)

	let entries = '';
	posts.forEach((post) => {
		entries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
				<button onclick = "editPost(${post.id})">Edit</button>
				<button onclick = "deletePost(${post.id})">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = entries
	console.log(document.querySelector('#div-post-entries'))
}

/*const showPosts = (posts) => {

	let entries = ''
	posts.forEach((post) => {
		entries += `
			<div id = "post-${post.id}">
				<h3 id = "post-title-${post.id}">${post.title}</h3>
				<p id = "post-body-${post.id}">${post.body}</p>
			</div>
		`
	})
	document.querySelector('#div-post-entries').innerHTML = entries
}*/

/*
	- We created function named 'showPosts' with an argument named 'posts'

	- We created a variable named 'entries' whcih we made into an empty string

	- We looped the posts argument in a forEach loop using 'post' as the arguement name

	- In this forEach loop, we created a new div with HTML elements

	- We used an addition operator in the forEach loop for the empty string variable ('entries') which then puts each iteration of the loop within the 'entries' variable

	- NOTE that the 'post' arguement in the forEach loop is the individual iteration of the entire 'posts' collection

	- We then targeted the <div></div> section in the index.html file using the document.querySelector('#div-post-entries') and we used the innerHTML property which we equated to the empty string variable, 'entries'

	- Since 'entries' now contains the iteration of the forEach loop with HTML elements using the attributes of the 'posts' arguement, each value will show the result with the HTML format created. 

	- We added a <button></button> with an attribute 'onclick', which activates the function 'editPost()' it is assigned to on click of the button (see the function below)
*/

// POST data on our API
document.querySelector('#form-add-post').addEventListener('submit', (event) => {
	// NOTE: When the submit event is used, we must add a parameter 'event' to the function to capture the properties of the event

	// to change the autoreload of the submit method; this prevents the auto refresh whenever the event occurs
	event.preventDefault();

	// POST Method
		// If we use the POST request, the fetch method will return the newly created document

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			
			title: document.querySelector('#txt-title').value,
			// this element is an 'input' type, therefore, its value will be the input it receives

			body: document.querySelector('#txt-body').value,
			// same logic as above applies

			userId: 1
		})
	})
	.then(response => response.json())
	.then(result => {
		console.log(result);
		alert('Post is successfully added')
		
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
		// these last two codes (= null) is just a way to empty the input fields after submission

	})
})

// EDIT Post Functionality
const editPost = (id) => {
	console.log(id)
	
	// let id = document.querySelector(`#post-${id}`).innerHTML -> this one works only if the attribute 'disabled' is not there

	let title = document.querySelector(`#post-title-${id}`).innerHTML
	console.log(title);

	let body = document.querySelector(`#post-body-${id}`).innerHTML
	console.log(body);

	// What happened here was we stored the title and the body of the iterated posts in the forEach loop above in the respective title and body variables

	document.querySelector('#txt-edit-id').value = id
	document.querySelector('#txt-edit-title').value = title
	document.querySelector('#txt-edit-body').value = body
	document.querySelector('#btn-submit-update').removeAttribute('disabled')

	// We reassigned the attributes to the respective querySelectors in the index.html file; this puts the texts into the input area in the HTML
}

// Updating the post (since this is a mock database, updates will only be seen in console)
document.querySelector('#form-edit-post').addEventListener('submit', (event) => {

	event.preventDefault() // to prevenet autoreload

	let id = document.querySelector('#txt-edit-id').value
	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {

		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			
			title: document.querySelector('#txt-edit-title').value,

			body: document.querySelector('#txt-edit-body').value,

			userId: 1
		})

	})
	.then(response => response.json())
	.then(result => {
		console.log(result);
		alert('The post is successfully updated')

		document.querySelector(`#post-title-${id}`).innerHTML = document.querySelector('#txt-edit-title').value

		document.querySelector(`#post-body-${id}`).innerHTML = document.querySelector('#txt-edit-body').value

		document.querySelector('#txt-edit-title').value = null
		document.querySelector('#txt-edit-body').value = null
		document.querySelector('#btn-submit-update').setAttribute('disabled', true)
	})
})

// Activity
const deletePost = (id) => {
	let element = document.querySelector(`#post-${id}`)
	element.remove()
}