// [Section] Document Object Model (DOM)
/*
	- DOM allows us to access or modify the properties of an html element in a webpage

	- It is a standard on how to get, change, add or delete HTML elements

	- We will be focusing only on managing forms with DOM
*/

/*let firstElement = document.querySelector('#txt-first-name');
console.log(firstElement)*/
/*
	- Using the querySelector will allow us to access/get the HTML element/s

	- CSS selectors to target specific element
		- id selector (#);
		- class selector (.)
		- tag/type selector (html tags);
		- universal selector (*)
		- attribute selector ([attributeName])
	
	- Two Types of Query Selectors:
		- querySelector
		- querySelectorAll
*/

// querySelector: selects only the first element with the '.full-name' class
let secondElement = document.querySelector('.full-name')

// querySelectorAll: selects all elements with the '.full-name' class
let thirdElement = document.querySelectorAll('.full-name')

console.log(secondElement)
console.log(thirdElement)

// getElement: works the same way as querySelector
let element = document.getElementById('fullName')
console.log(element)

// getElementsByClassName: works the same way as querySelectorAll
let elementClass = document.getElementsByClassName('full-name')
console.log(elementClass)

// [Section] Event Listeners
/*
	- Whenever a user interacts with a webpage, an event occurs
	- Working with events is a large part of creating interactivity in a webpage
	- A specific function will be invoked if the event happens
*/

let fullName = document.querySelector('#fullName')
// console.log(fullName.value) -> this will return an undefined, since .value will work only on ceratin input fields

console.log(fullName.innerHTML) // .innerHTML -> this will console the values and codes inside the element it is assigned to (in this case, the span element in HTML)

let firstElement = document.querySelector('#txt-first-name');
console.log(firstElement)

firstElement.addEventListener('keydown', ()=>{
	console.log(firstElement.value);
	let valueInput = firstElement.value;

	fullName.innerHTML = `${firstElement.value} ${txtLastName.value}`
})

/*
.addEventListener() has 2 arguments: 
	- the 1st argument is a string for identifying the event that you want to happen;

	- the 2nd elevent is a function that the listener will invoke once the specified event occurs;
*/

let txtLastName = document.querySelector('#txt-last-name')

txtLastName.addEventListener('keyup', ()=>{
	console.log(txtLastName.value);
	fullName.innerHTML = `${firstElement.value} ${txtLastName.value}` 
})

let color = document.querySelector('#text-color');

color.addEventListener('change', ()=>{
	fullName.style.color = color.value
})

/*
	NOTES: 
	
	- the 'change' event listener is used for dropdown elements, in such a way that the value changes according to the option chosen

	- the '.syle.color' property is used to get the color of the text content within an HTML element; (syntax: element.style.color = "colorYouWant")

	- What happened here basically is that you selected the fullName variable (as declared above), and attached a '.style.color' property 

	- the 'fullName.style.color' property was then given the value of 'color.value', wherein 'color' is a dropdown variable with a selected query
*/